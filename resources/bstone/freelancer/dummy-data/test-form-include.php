<?php
/**
 * Caldera Forms - PHP Export 
 * Test Form 
 * @see https://calderaforms.com/doc/exporting-caldera-forms/ 
 * @version    1.8.7
 * @license   GPL-2.0+
 * 
 */

 /*

import_widget_settings
 */

 

	

/**
                     * Hooks to load form.
                     * Remove "caldera_forms_admin_forms" if you do not want this form to show in admin entry viewer
                     */
                    add_filter( "caldera_forms_get_forms", "slug_register_caldera_forms_testform" );
                    add_filter( "caldera_forms_admin_forms", "slug_register_caldera_forms_testform" );
                    /**
                     * Add form to front-end and admin
                     *
                     * @param array $forms All registered forms
                     *
                     * @return array
                     */
                    function slug_register_caldera_forms_testform( $forms ) {
                        $forms["test-form"] = apply_filters( "caldera_forms_get_form-test-form", array() );
                        return $forms;
                    };

/**
 * Filter form request to include form structure to be rendered
 *
 * @since 1.3.1
 *
 * @param $form array form structure
 */
add_filter( 'caldera_forms_get_form-test-form', function( $form ){
 return array(
  'name' => 'Test Form',
  'description' => '',
  'db_support' => 1,
  'hide_form' => 1,
  'success' => 'Form has been successfully submitted. Thank you.',
  'avatar_field' => 'fld_6009157',
  'form_ajax' => 1,
  'layout_grid' => 
  array(
    'fields' => 
    array(
      'fld_29462' => '1:1',
      'fld_8768091' => '2:1',
      'fld_9970286' => '2:2',
      'fld_6009157' => '2:3',
      'fld_2758980' => '3:1',
      'fld_7683514' => '4:1',
      'fld_7908577' => '5:1',
    ),
    'structure' => '12|4:4:4|12|12|12',
  ),
  'fields' => 
  array(
    'fld_29462' => 
    array(
      'ID' => 'fld_29462',
      'type' => 'html',
      'label' => 'header',
      'slug' => 'header',
      'caption' => '',
      'config' => 
      array(
        'custom_class' => '',
        'default' => '<h2>Your Details</h2>
<p>Let us know how to get back to you.</p>
<hr>',
      ),
      'conditions' => 
      array(
        'type' => '',
      ),
    ),
    'fld_8768091' => 
    array(
      'ID' => 'fld_8768091',
      'type' => 'text',
      'label' => 'First Name',
      'slug' => 'first_name',
      'required' => '1',
      'caption' => '',
      'config' => 
      array(
        'custom_class' => '',
        'placeholder' => '',
        'default' => '',
        'mask' => '',
      ),
      'conditions' => 
      array(
        'type' => '',
      ),
    ),
    'fld_9970286' => 
    array(
      'ID' => 'fld_9970286',
      'type' => 'text',
      'label' => 'Last Name',
      'slug' => 'last_name',
      'required' => '1',
      'caption' => '',
      'config' => 
      array(
        'custom_class' => '',
        'placeholder' => '',
        'default' => '',
        'mask' => '',
      ),
      'conditions' => 
      array(
        'type' => '',
      ),
    ),
    'fld_6009157' => 
    array(
      'ID' => 'fld_6009157',
      'type' => 'email',
      'label' => 'Email Address',
      'slug' => 'email_address',
      'required' => '1',
      'caption' => '',
      'config' => 
      array(
        'custom_class' => '',
        'placeholder' => '',
        'default' => '',
      ),
      'conditions' => 
      array(
        'type' => '',
      ),
    ),
    'fld_2758980' => 
    array(
      'ID' => 'fld_2758980',
      'type' => 'html',
      'label' => 'Message',
      'slug' => 'message',
      'caption' => '',
      'config' => 
      array(
        'custom_class' => '',
        'default' => '<h2>How can we help?</h2>
<p>Feel free to ask a question or simply leave a comment.</p>
<hr>',
      ),
      'conditions' => 
      array(
        'type' => '',
      ),
    ),
    'fld_7683514' => 
    array(
      'ID' => 'fld_7683514',
      'type' => 'paragraph',
      'label' => 'Comments / Questions',
      'slug' => 'comments_questions',
      'required' => '1',
      'caption' => '',
      'config' => 
      array(
        'custom_class' => '',
        'placeholder' => '',
        'rows' => '7',
        'default' => '',
      ),
      'conditions' => 
      array(
        'type' => '',
      ),
    ),
    'fld_7908577' => 
    array(
      'ID' => 'fld_7908577',
      'type' => 'button',
      'label' => 'Send Message',
      'slug' => 'submit',
      'caption' => '',
      'config' => 
      array(
        'custom_class' => '',
        'type' => 'submit',
        'class' => 'btn btn-default',
      ),
      'conditions' => 
      array(
        'type' => '',
      ),
    ),
  ),
  'page_names' => 
  array(
    0 => 'Page 1',
  ),
  'processors' => 
  array(
    'fp_17689566' => 
    array(
      'ID' => 'fp_17689566',
      'type' => 'auto_responder',
      'config' => 
      array(
        'sender_name' => 'Freelancer',
        'sender_email' => 'nasir.ist@hotmail.com',
        'subject' => 'Thank you for contacting us',
        'recipient_name' => '%first_name% %last_name%',
        'recipient_email' => '%email_address%',
        'message' => 'Hi %recipient_name%.
Thanks for your email.
We\'ll get back to you as soon as possible!
Here\'s a summary of your message:
------------------------
{summary}',
      ),
      'conditions' => 
      array(
        'type' => '',
      ),
    ),
  ),
  'settings' => 
  array(
    'responsive' => 
    array(
      'break_point' => 'sm',
    ),
  ),
  'mailer' => 
  array(
    'on_insert' => 1,
  ),
  'ID' => 'test-form',
  'check_honey' => 1,
  'template' => 'starter_contact_form',
  'db_id' => '2',
  'type' => 'primary',
  '_external_form' => 1,
);
} );
